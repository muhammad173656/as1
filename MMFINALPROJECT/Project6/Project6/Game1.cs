﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Media;

namespace SpeedyCar
{

    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch SpriteBatch;
        Song song;

        //The scrolling road
        Texture2D Road;           //800x600  ... so we change screen size to match

        float RoadScrollSpeed;          //the speed of the vertical scrolling road
        int[] newRoad = new int[3]; //we are going to piece together the road 3 times

        //The car driving on the road and its position
        Texture2D Car;
        Rectangle CarPos; //calculated in StartGame() below

        //We are going to use the keyboard to move the car from left to right and right to left (not up and down)
        KeyboardState mPreviousKeyboardState;
        int laneSwitch;

        //We are going to place a hazard and random locations on the scrolling road
        Texture2D Obstacles;
        Rectangle ObstaclesPos; //calculated in StartGame() below

        Random Random = new Random();

        //this will keep track of the number of hazards we have passed sucessfully without hitting
        int HazardsPassed;

        SpriteFont Font;


        enum State
        {
            Running,
            Crash
        }

        State mCurrentState;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;

            this.Window.Position = new Point(400, 200);

            this.IsMouseVisible = true;
        }


        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            StartGame();            //instead of declaring all the initializations here we call a separate method


        }

        protected void StartGame()
        {
            //piece together the scrolling road ... placing it up and offscreen... 
            //we will later move it DOWNWARD on the screen ... it will kind of feel like you are driving forward   
            newRoad[0] = 0;
            newRoad[1] = newRoad[0] + -Road.Height + 2;
            newRoad[2] = newRoad[1] + -Road.Height + 2;

            RoadScrollSpeed = 0.3F;


            //Actual dimensions of car is 501x737 ... to big for either screen ... so 
            //Couldn't use Vector2 here to position since we also needed to scale + we use it for our collision detection

            CarPos = new Rectangle(280, 440, (int)(Car.Width * 0.2f), (int)(Car.Height * 0.2f));


            //This is how much OVER we move the car to avoid obstacles when we press the spacebar...we go laneswhitch if we are on the right side
            laneSwitch = 160;


            //original dimensions of obstacles is 256x256
            //Couldn't use Vector2 here to position since we also needed to scale + we use it for our collision detection

            ObstaclesPos = new Rectangle(275, -Obstacles.Height, (int)(Obstacles.Width * 0.4f), (int)(Obstacles.Height * 0.4f));

            //obstacles moves down screen ... position updated in UpdateHazard() method below

            HazardsPassed = 0;

            mCurrentState = State.Running;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            Road = Content.Load<Texture2D>("Road");
            Car = Content.Load<Texture2D>("Car");
            Obstacles = Content.Load<Texture2D>("Hazard");

            Font = Content.Load<SpriteFont>("Font");

            this.song = Content.Load<Song>("prepare");
            MediaPlayer.Play(song);
            //  Uncomment the following line will also loop the song
            //  MediaPlayer.IsRepeating = true;
            //MediaPlayer.MediaStateChanged += MediaPlayer_MediaStateChanged;
        }


        protected override void UnloadContent()
        {

        }


        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            KeyboardState CurrentKeyState = Keyboard.GetState();

            if (CurrentKeyState.IsKeyDown(Keys.Escape) == true)
            {
                this.Exit();
            }

            switch (mCurrentState)
            {
                case State.Running:
                    {
                        if ((CurrentKeyState.IsKeyDown(Keys.Space) == true && mPreviousKeyboardState.IsKeyDown(Keys.Space) == false))

                        {
                            CarPos.X += laneSwitch; //move car over
                            laneSwitch *= -1;             //reverse direction 
                        }

                        RdScroll(gameTime);

                        if (ObstaclesPos.Intersects(CarPos) == false)
                        {
                            UpdateObstacles(gameTime);
                        }
                        else
                        {
                            mCurrentState = State.Crash;
                        }

                        break;
                    }
                case State.Crash:
                    {
                        if (CurrentKeyState.IsKeyDown(Keys.Enter) == true)
                        {
                            StartGame();
                        }
                        break;
                    }
            }

            //make current keyboard state old keyboard state
            mPreviousKeyboardState = CurrentKeyState;


            base.Update(gameTime);
        }

        private void RdScroll(GameTime theTime)
        {
            //Loop the road when it reaches the bottom
            for (int Index = 0; Index < newRoad.Length; Index++)  //remember  is an array ... so Length is the size of the array 3
            {
                //if a road gets to the bottom of the screen
                if (newRoad[Index] >= this.Window.ClientBounds.Height)
                {
                    //now found the smallest [] at this moment ... position will be at aLastRoadIndex
                    int aLastRoadIndex = Index;
                    for (int aCounter = 0; aCounter < newRoad.Length; aCounter++)
                    {
                        while (newRoad[aCounter] < newRoad[aLastRoadIndex])
                        {
                            aLastRoadIndex = aCounter;
                        }
                    }

                    newRoad[Index] = newRoad[aLastRoadIndex] - Road.Height + 2;

                }
            }



            for (int aIndex = 0; aIndex < newRoad.Length; aIndex++)
            {
                newRoad[aIndex] += (int)(RoadScrollSpeed * theTime.ElapsedGameTime.TotalMilliseconds); // 0.3f
            }
            //Recall: ElapsedGameTime.TotalMilliseconds is ... the amount of elapsed game time since the last update.
        }

        private void UpdateObstacles(GameTime theTime)
        {
            ObstaclesPos.Y += (int)(RoadScrollSpeed * theTime.ElapsedGameTime.TotalMilliseconds); //move hazard down the screen

            while (ObstaclesPos.Y > this.Window.ClientBounds.Height)  //if hazard reaches bottom of screen
            {
                ObstaclesPos.X = 275;                              //reposition either on left or right side
                if (Random.Next(1, 3) == 2)
                {
                    ObstaclesPos.X = 440;
                }

                ObstaclesPos.Y = -Obstacles.Height;   //offscreen for a second

                HazardsPassed += 1;                   //update points ... reached bottom without collision
                RoadScrollSpeed += 0.1F;                    //update speed..faster and faster
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.SaddleBrown);

            // TODO: Add your drawing code here
            SpriteBatch.Begin();

            //draw the road 3 times ... one will be onscreen the other two above the screen
            for (int aIndex = 0; aIndex < newRoad.Length; aIndex++)
            {
                SpriteBatch.Draw(Road, new Vector2(0, newRoad[aIndex]), Color.White);
            }

            SpriteBatch.Draw(Car, CarPos, Color.White);
            SpriteBatch.Draw(Obstacles, ObstaclesPos, Color.White);

            SpriteBatch.DrawString(Font, "Hazards: " + HazardsPassed.ToString(), new Vector2(5, 25), Color.White,
                0, new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);

            if (mCurrentState == State.Crash)
            {
                SpriteBatch.DrawString(Font, "Crash!", new Vector2(5, 200), Color.White, 0, new Vector2(0, 0), 1.0f,
                    SpriteEffects.None, 0);
                SpriteBatch.DrawString(Font, "'Enter' to play again.", new Vector2(5, 230), Color.White, 0,
                    new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
                SpriteBatch.DrawString(Font, "'Escape' to exit.", new Vector2(5, 260), Color.White, 0,
                    new Vector2(0, 0), 1.0f, SpriteEffects.None, 0);
            }

            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
