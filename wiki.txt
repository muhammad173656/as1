The goal of private repositories is to preserve your code without exposing it to the public. 
Such as programmes that are now proprietary to you and that you do not wish to distribute.
 In essence, it's merely a location to save your secret code in a remote repository.